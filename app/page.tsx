import Link from "next/link";

import { Button } from "@design-system/ui/button";

export default function Page() {
  return (
    <div className="flex h-screen w-screen flex-col items-center justify-center gap-3">
      <h1 className="text-2xl font-semibold">Welcome to the Delivery App!</h1>
      <Link href="/users">
        <Button>Start ordering</Button>
      </Link>
    </div>
  );
}
