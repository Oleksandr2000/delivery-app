module.exports = {
  parser: "@typescript-eslint/parser",
  extends: ["next/core-web-vitals", "prettier", "plugin:@typescript-eslint/recommended"],
  plugins: ["simple-import-sort"],
  parserOptions: {
    ecmaVersion: 2024,
    ecmaFeatures: {
      jsx: true,
    },
  },
  rules: {
    curly: ["error", "all"],
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/no-shadow": ["error"],
    "import/no-anonymous-default-export": "off",
    "simple-import-sort/imports": [
      "error",
      {
        groups: [
          ["^\\u0000"],
          // Node.js builtins prefixed with `node:`.
          ["^node:"],
          // Packages.
          // Things that start with a letter (or digit or underscore), or `@` followed by a letter.
          ["^@?\\w.*\\u0000$", "^@?\\w"],
          // Internal packages.
          ["^(@components|@design-system|@modules|@layouts|@services)(/.*|$)"],
          // Parent imports. Put `..` last.
          ["^\\.\\.(?!/?$)", "^\\.\\./?$", "^\\./(?=.*/)(?!/?$)", "^\\.(?!/?$)", "^\\./?$"],
          // Other relative imports. Put same-folder imports and `.` last.
          [],
        ],
      },
    ],
    "simple-import-sort/exports": "error",
  },
};
