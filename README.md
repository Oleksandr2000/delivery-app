# Delivery App

The deployed version is available at: https://delivery-app-silk.vercel.app/

## How to run

1. Install node 18+ version.
2. `corepack enable` for using yarn v4.
3. Install dependencies: `yarn`.
4. Run `yarn start:dev` and visit https://localhost:3000.

## Linting

- `yarn lint` - checks for eslint, prettier, and TS errors
- `yarn format` - runs prettier code style checks

CI/CD pipeline automatically runs linter checks on push.
