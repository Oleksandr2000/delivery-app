/**
 * Function for converting zoned date without time into correct UTC date.
 * @param date - Local date without time.
 * @returns UTC Date without time.
 */
export const inputDateToUTC = (date: Date) => {
  const tzOffset = date.getTimezoneOffset() / 60;

  return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours() - tzOffset));
};
