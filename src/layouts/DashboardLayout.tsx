"use client";
import { Database, Users } from "lucide-react";
import Link from "next/link";
import React, { ReactNode } from "react";

import { Button } from "@design-system/ui/button";
import { Toaster } from "@design-system/ui/sonner";

interface Props {
  children: ReactNode;
}

const DashboardLayout = ({ children }: Props) => {
  return (
    <>
      <main className="flex">
        <nav className="flex w-52 flex-col gap-2 bg-muted px-3 py-6">
          <Link href="/users">
            <Button variant="link" className="flex items-center gap-1 text-sm font-medium">
              <Users className="h-4 w-4" />
              Users
            </Button>
          </Link>
          <Link href="/requests">
            <Button variant="link" className="flex items-center gap-1 text-sm font-medium">
              <Database className="h-4 w-4" />
              Requests
            </Button>
          </Link>
        </nav>
        <section className="min-h-screen w-full px-4 py-6">{children}</section>
      </main>
      <Toaster />
    </>
  );
};

export default DashboardLayout;
