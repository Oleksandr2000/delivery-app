import { inputDateToUTC } from "@services/tz.service";

import { DeliveryDTO, IDelivery, IOrder, OrderDTO } from "./request.model";

export const normalizeOrder = (order: IOrder): OrderDTO => ({
  ...order,
  dispatchDate: new Date(order.dispatchDate),
});

export const serializeOrderValues = (order: OrderDTO & { id?: number }) => ({
  ...order,
  dispatchDate: inputDateToUTC(order.dispatchDate),
});

export const normalizeDelivery = (delivery: IDelivery): DeliveryDTO => ({
  ...delivery,
  dispatchDate: new Date(delivery.dispatchDate),
});

export const serializeDeliveryValues = (delivery: DeliveryDTO & { id?: number }) => ({
  ...delivery,
  dispatchDate: inputDateToUTC(delivery.dispatchDate),
});
