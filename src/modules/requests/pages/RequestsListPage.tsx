"use server";
import DeliveryTable from "../components/DeliveryTable";
import OrdersTable from "../components/OrdersTable";
import { fetchDeliveries, fetchOrders } from "../request.actions";
import { IDelivery, IOrder } from "../request.model";

const RequestsListPage = async () => {
  const [{ data: deliveries }, { data: orders }] = await Promise.all([fetchDeliveries(), fetchOrders()]);

  return (
    <>
      <h2 className="mb-3 text-xl font-semibold">Orders</h2>
      <OrdersTable orders={(orders as unknown as IOrder[]) || []} />
      <h2 className="mb-3 mt-6 text-xl font-semibold">Deliveries</h2>
      <DeliveryTable deliveries={(deliveries as unknown as IDelivery[]) || []} />
    </>
  );
};

export default RequestsListPage;
