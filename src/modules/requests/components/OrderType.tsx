import React from "react";

import { cn } from "@design-system/lib/utils";

import { ORDER_TYPES } from "../request.model";

interface Props {
  type: ORDER_TYPES | null;
}

const commonClasses = "inline-flex items-center rounded-full border px-2.5 py-0.5 text-xs font-semibold capitalize";

const OrderType = ({ type }: Props) => {
  switch (type) {
    case ORDER_TYPES.CLOTHES:
      return <span className={cn(commonClasses, "border border-green-500 bg-green-100 text-green-500")}>{type}</span>;
    case ORDER_TYPES.DRINKS:
      return (
        <span className={cn(commonClasses, "border border-orange-500 bg-orange-100 text-orange-500")}>{type}</span>
      );
    case ORDER_TYPES.GADGETS:
      return <span className={cn(commonClasses, "border border-cyan-500 bg-cyan-100 text-cyan-500")}>{type}</span>;
    case ORDER_TYPES.MEDICINES:
      return <span className={cn(commonClasses, "border border-blue-500 bg-blue-100 text-blue-500")}>{type}</span>;
    case ORDER_TYPES.OTHER:
      return (
        <span className={cn(commonClasses, "border border-violet-500 bg-violet-100 text-violet-500")}>{type}</span>
      );
    default:
      return <span className={cn(commonClasses)}>{type}</span>;
  }
};

export default OrderType;
