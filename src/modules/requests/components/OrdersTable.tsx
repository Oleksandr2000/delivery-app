"use client";
import {
  ColumnDef,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  PaginationState,
  RowSelectionState,
  SortingState,
  useReactTable,
} from "@tanstack/react-table";
import { format } from "date-fns";
import { ArrowDownUp, ArrowUpDown, Edit, PlusIcon } from "lucide-react";
import Link from "next/link";
import { useMemo, useState } from "react";

import DataTable from "@components/DataTable";
import { Button } from "@design-system/ui/button";
import { Checkbox } from "@design-system/ui/checkbox";
import { Input } from "@design-system/ui/input";
import { Tooltip, TooltipContent, TooltipProvider, TooltipTrigger } from "@design-system/ui/tooltip";

import { IOrder, ORDER_TYPES } from "../request.model";
import OrderType from "./OrderType";

interface Props {
  orders: IOrder[];
  addOrderPath?: string;
  customerId?: number;
}

const OrdersTable = ({ orders, addOrderPath, customerId }: Props) => {
  const [globalFilter, setGlobalSearch] = useState("");
  const [sorting, setSorting] = useState<SortingState>([]);
  const [pagination, setPagination] = useState<PaginationState>({ pageIndex: 0, pageSize: 10 });
  const [rowSelection, setRowSelection] = useState<RowSelectionState>({});

  const columns = useMemo(
    () => [
      {
        id: "id",
        accessorKey: "id",
        header: ({ table }) => (
          <Checkbox
            checked={table.getIsAllPageRowsSelected()}
            onCheckedChange={(value) => table.toggleAllPageRowsSelected(!!value)}
          />
        ),
        cell: ({ row }) => (
          <Checkbox
            checked={row.getIsSelected()}
            disabled={!row.getCanSelect()}
            onCheckedChange={(value) => row.toggleSelected(!!value)}
          />
        ),
      },
      {
        id: "dispatchDate",
        accessorFn: (row) => format(new Date(row.dispatchDate), "dd MMMM yyyy"),
        header: ({ column }) => {
          return (
            <span
              className="flex items-center gap-1"
              onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
            >
              Dispatch Date
              {column.getIsSorted() === "asc" ? (
                <ArrowUpDown className="h-4 w-4" />
              ) : (
                <ArrowDownUp className="h-4 w-4" />
              )}
            </span>
          );
        },
      },
      {
        id: "fromCity",
        accessorKey: "fromCity",
        header: ({ column }) => {
          return (
            <span
              className="flex items-center gap-1"
              onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
            >
              From City
              {column.getIsSorted() === "asc" ? (
                <ArrowUpDown className="h-4 w-4" />
              ) : (
                <ArrowDownUp className="h-4 w-4" />
              )}
            </span>
          );
        },
      },
      {
        id: "toCity",
        accessorKey: "toCity",
        header: ({ column }) => {
          return (
            <span
              className="flex items-center gap-1"
              onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
            >
              To City
              {column.getIsSorted() === "asc" ? (
                <ArrowUpDown className="h-4 w-4" />
              ) : (
                <ArrowDownUp className="h-4 w-4" />
              )}
            </span>
          );
        },
      },
      !addOrderPath
        ? {
            id: "customers",
            accessorKey: "customers",
            cell: ({ getValue }) => (
              <Link href={`/users/${(getValue() as IOrder["customers"]).id}/requests`} className="text-blue-400">
                {`${(getValue() as IOrder["customers"]).firstname} ${(getValue() as IOrder["customers"]).surname}`}
              </Link>
            ),
            header: ({ column }) => {
              return (
                <span
                  className="flex items-center gap-1"
                  onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
                >
                  Sender
                  {column.getIsSorted() === "asc" ? (
                    <ArrowUpDown className="h-4 w-4" />
                  ) : (
                    <ArrowDownUp className="h-4 w-4" />
                  )}
                </span>
              );
            },
          }
        : null,
      {
        id: "type",
        accessorKey: "type",
        cell: ({ getValue }) => <OrderType type={getValue() as ORDER_TYPES} />,
        header: ({ column }) => {
          return (
            <span
              className="flex items-center gap-1"
              onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
            >
              Type
              {column.getIsSorted() === "asc" ? (
                <ArrowUpDown className="h-4 w-4" />
              ) : (
                <ArrowDownUp className="h-4 w-4" />
              )}
            </span>
          );
        },
      },
      {
        id: "description",
        accessorKey: "description",
        header: "Description",
        cell: ({ getValue }) => (
          <TooltipProvider>
            <Tooltip>
              <TooltipTrigger className="max-w-60 truncate">{getValue() as string}</TooltipTrigger>
              <TooltipContent>
                <p>{getValue() as string}</p>
              </TooltipContent>
            </Tooltip>
          </TooltipProvider>
        ),
      },
      {
        id: "id",
        accessorKey: "id",
        cell: ({ row }) => (
          <Link
            href={`/users/${customerId || (row.getValue("customers") as IOrder["customers"])?.id}/edit/${row.getValue("id")}/order`}
          >
            <Edit className="ml-auto mr-4 h-4 w-4" />
          </Link>
        ),
        header: () => <span className="float-right mr-4">Edit</span>,
      },
    ],
    [addOrderPath, customerId],
  ) as ColumnDef<IOrder>[];

  const table = useReactTable({
    data: orders,
    columns: columns.filter(Boolean) as ColumnDef<IOrder>[],
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    onSortingChange: setSorting,
    onPaginationChange: setPagination,
    onGlobalFilterChange: setGlobalSearch,
    onRowSelectionChange: setRowSelection,
    state: {
      sorting,
      pagination,
      globalFilter,
      rowSelection,
    },
  });

  return (
    <>
      <div className="mb-3 flex items-center justify-between">
        <Input
          placeholder="Search..."
          value={globalFilter}
          onChange={(e) => setGlobalSearch(e.target.value)}
          className="w-80"
        />
        {addOrderPath && (
          <Link href={addOrderPath}>
            <Button size="sm">
              <PlusIcon className="mr-1 h-4 w-4" />
              Add order
            </Button>
          </Link>
        )}
      </div>
      <DataTable table={table} />
    </>
  );
};

export default OrdersTable;
