import { IUser } from "@modules/users/user.model";

export interface IBaseRequest {
  id: number;
  senderId: number;
  fromCity: string;
  toCity: string;
  dispatchDate: string;
  customers: Pick<IUser, "id" | "firstname" | "surname">;
}

export enum ORDER_TYPES {
  GADGETS = "gadgets",
  DRINKS = "drinks",
  CLOTHES = "clothes",
  MEDICINES = "medicines",
  OTHER = "other",
}
export const ORDER_TYPES_METADATA = [
  { value: ORDER_TYPES.GADGETS, label: "Gadgets" },
  { value: ORDER_TYPES.DRINKS, label: "Drinks" },
  { value: ORDER_TYPES.CLOTHES, label: "Clothes" },
  { value: ORDER_TYPES.MEDICINES, label: "Medicines" },
  { value: ORDER_TYPES.OTHER, label: "Other" },
];

export interface IOrder extends IBaseRequest {
  description?: string;
  type: ORDER_TYPES | null;
}

export interface IDelivery extends IBaseRequest {}

export type DeliveryDTO = Omit<IDelivery, "id" | "customers" | "dispatchDate"> & {
  id?: number;
  dispatchDate: Date;
};

export const getDefaultDeliveryValues = (senderId: number): DeliveryDTO => ({
  toCity: "",
  fromCity: "",
  dispatchDate: new Date(),
  senderId,
});

export type OrderDTO = Omit<IOrder, "id" | "customers" | "dispatchDate"> & {
  id?: number;
  dispatchDate: Date;
};

export const getDefaultOrderValues = (senderId: number): OrderDTO => ({
  toCity: "",
  fromCity: "",
  dispatchDate: new Date(),
  description: "",
  type: null,
  senderId,
});
