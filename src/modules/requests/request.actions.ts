"use server";

import { PostgrestSingleResponse } from "@supabase/supabase-js";
import { revalidatePath } from "next/cache";

import supabaseClient from "@services/supabase.service";

import { DeliveryDTO, IDelivery, IOrder, OrderDTO } from "./request.model";
import { serializeDeliveryValues, serializeOrderValues } from "./request.utils";

export const createDelivery = async (delivery: DeliveryDTO) => {
  const res = await supabaseClient.from("deliveries").insert(serializeDeliveryValues(delivery));
  revalidatePath(`/users/[id]/requests`, "page");
  revalidatePath("/requests", "page");
  return res;
};

export const updateDelivery = async (delivery: DeliveryDTO, deliveryId: number) => {
  const res = await supabaseClient.from("deliveries").update(serializeDeliveryValues(delivery)).eq("id", deliveryId);
  revalidatePath(`/users/[id]/edit/[rid]/delivery`, "page");
  revalidatePath(`/users/[id]/requests`, "page");
  revalidatePath("/requests", "page");
  return res;
};

export const createOrder = async (order: OrderDTO) => {
  const res = await supabaseClient.from("orders").insert(serializeOrderValues(order));
  revalidatePath(`/users/[id]/requests`, "page");
  revalidatePath("/requests");
  return res;
};

export const updateOrder = async (order: OrderDTO, orderId: number) => {
  const res = await supabaseClient.from("orders").update(serializeOrderValues(order)).eq("id", orderId);
  revalidatePath(`/users/[id]/edit/[rid]/order`, "page");
  revalidatePath(`/users/[id]/requests`, "page");
  revalidatePath("/requests");
  return res;
};

export const fetchOrders = async (): Promise<PostgrestSingleResponse<IOrder[]>> =>
  await supabaseClient
    .from("orders")
    .select(`id, toCity, fromCity, dispatchDate, type, description, customers (id, firstname, surname)`);

export const fetchDeliveries = async (): Promise<PostgrestSingleResponse<IDelivery[]>> =>
  await supabaseClient
    .from("deliveries")
    .select(`id, toCity, fromCity, dispatchDate, customers (id, firstname, surname)`);

export const fetchUserOrders = async (userId: number): Promise<PostgrestSingleResponse<IOrder[]>> =>
  await supabaseClient.from("orders").select("*").eq("senderId", userId);

export const fetchUserDeliveries = async (userId: number): Promise<PostgrestSingleResponse<IDelivery[]>> =>
  await supabaseClient.from("deliveries").select("*").eq("senderId", userId);

export const fetchOrderById = async (id: number): Promise<PostgrestSingleResponse<IOrder[]>> =>
  await supabaseClient.from("orders").select("*").eq("id", id);

export const fetchDeliveryById = async (id: number): Promise<PostgrestSingleResponse<IDelivery[]>> =>
  await supabaseClient.from("deliveries").select("*").eq("id", id);
