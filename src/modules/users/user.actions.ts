import { PostgrestSingleResponse } from "@supabase/supabase-js";

import supabaseClient from "@services/supabase.service";

import { IUser } from "./user.model";

export const fetchCustomerById = async (id: number): Promise<PostgrestSingleResponse<IUser[]>> =>
  await supabaseClient.from("customers").select("*").eq("id", id);
