"use client";
import { yupResolver } from "@hookform/resolvers/yup";
import { format } from "date-fns";
import { useRouter } from "next/navigation";
import React from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { toast } from "sonner";
import * as yup from "yup";

import { DatePicker } from "@components/DatePicker";
import { Button } from "@design-system/ui/button";
import { Form, FormControl, FormField, FormItem, FormLabel, FormMessage } from "@design-system/ui/form";
import { Input } from "@design-system/ui/input";
import { createDelivery, updateDelivery } from "@modules/requests/request.actions";
import { DeliveryDTO } from "@modules/requests/request.model";

interface Props {
  defaultValues: DeliveryDTO;
}

const schema = yup.object({
  fromCity: yup.string().required("Enter dispatch city"),
  toCity: yup.string().required("Enter city of arrival"),
  dispatchDate: yup.date().required("Enter dispatch date"),
  senderId: yup.number().required(),
});

const UserDeliveryForm = ({ defaultValues }: Props) => {
  const { push } = useRouter();
  const isEdit = defaultValues?.id;

  const form = useForm<DeliveryDTO>({
    defaultValues,
    resolver: yupResolver<Omit<DeliveryDTO, "id">>(schema),
  });

  const {
    handleSubmit,
    control,
    formState: { isDirty },
  } = form;

  const onSubmit: SubmitHandler<DeliveryDTO> = async (data) => {
    let res;
    if (!isEdit) {
      res = await createDelivery(data);
    } else {
      if (!defaultValues.id) {
        return toast("Provide delivery ID");
      }
      res = await updateDelivery(data, defaultValues.id);
    }

    if (!res.error) {
      toast(`Delivery request has been ${isEdit ? "updated" : "created"}.`, {
        description: `Dispatch date - ${format(data.dispatchDate, "dd MMMM yyyy")}`,
        action: {
          label: "Undo",
          onClick: () => {},
        },
      });
      push(`/users/${defaultValues.senderId}/requests`);
    } else {
      toast(res.error.message);
    }
  };

  return (
    <Form {...form}>
      <form className="flex w-96 flex-col gap-6" onSubmit={handleSubmit(onSubmit)}>
        <FormField
          name="fromCity"
          control={control}
          render={({ field }) => (
            <FormItem>
              <FormLabel>From City</FormLabel>
              <FormControl>
                <Input placeholder="From city" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          name="toCity"
          control={control}
          render={({ field }) => (
            <FormItem>
              <FormLabel>To City</FormLabel>
              <FormControl>
                <Input placeholder="To city" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          name="dispatchDate"
          control={control}
          render={({ field }) => (
            <FormItem>
              <FormLabel>Dispatch Date</FormLabel>
              <FormControl>
                <DatePicker value={field.value} onChange={field.onChange} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <div className="flex items-center justify-end gap-2">
          <Button type="submit" className="px-5" disabled={!isDirty}>
            Save
          </Button>
        </div>
      </form>
    </Form>
  );
};

export default UserDeliveryForm;
