"use client";
import {
  ColumnDef,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  PaginationState,
  RowSelectionState,
  SortingState,
  useReactTable,
} from "@tanstack/react-table";
import { ArrowDownUp, ArrowUpDown, Ellipsis, Eye, PlusIcon } from "lucide-react";
import Link from "next/link";
import { useState } from "react";

import DataTable from "@components/DataTable";
import { Button } from "@design-system/ui/button";
import { Checkbox } from "@design-system/ui/checkbox";
import { Input } from "@design-system/ui/input";
import { Popover, PopoverContent, PopoverTrigger } from "@design-system/ui/popover";

import { IUser } from "../user.model";

interface Props {
  users: IUser[];
}

export const columns: ColumnDef<IUser>[] = [
  {
    id: "id",
    accessorKey: "id",
    header: ({ table }) => (
      <Checkbox
        checked={table.getIsAllPageRowsSelected()}
        onCheckedChange={(value) => table.toggleAllPageRowsSelected(!!value)}
      />
    ),
    cell: ({ row }) => (
      <Checkbox
        checked={row.getIsSelected()}
        disabled={!row.getCanSelect()}
        onCheckedChange={(value) => row.toggleSelected(!!value)}
      />
    ),
  },
  {
    id: "name",
    accessorFn: (row) => `${row.firstname} ${row.surname}`,
    header: ({ column }) => {
      return (
        <span className="flex items-center gap-1" onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}>
          Name
          {column.getIsSorted() === "asc" ? <ArrowUpDown className="h-4 w-4" /> : <ArrowDownUp className="h-4 w-4" />}
        </span>
      );
    },
  },
  {
    id: "city",
    accessorKey: "city",
    header: ({ column }) => {
      return (
        <span className="flex items-center gap-1" onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}>
          City
          {column.getIsSorted() === "asc" ? <ArrowUpDown className="h-4 w-4" /> : <ArrowDownUp className="h-4 w-4" />}
        </span>
      );
    },
  },
  {
    id: "email",
    accessorKey: "email",
    header: ({ column }) => {
      return (
        <span className="flex items-center gap-1" onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}>
          Email
          {column.getIsSorted() === "asc" ? <ArrowUpDown className="h-4 w-4" /> : <ArrowDownUp className="h-4 w-4" />}
        </span>
      );
    },
  },
  {
    id: "phone",
    accessorKey: "phone",
  },
  {
    id: "id",
    accessorKey: "id",
    header: "More",
    cell: ({ getValue }) => (
      <Popover>
        <PopoverTrigger>
          <Ellipsis className="h-4 w-4" />
        </PopoverTrigger>
        <PopoverContent className="w-auto p-2">
          <div className="flex flex-col gap-2">
            <Link href={`users/${getValue()}/requests`}>
              <Button variant="ghost" className="w-full">
                <Eye className="mr-2 h-4 w-4" />
                View Requests
              </Button>
            </Link>
            <Link href={`users/${getValue()}/create`}>
              <Button variant="ghost" className="w-full">
                <PlusIcon className="mr-2 h-4 w-4" />
                Add Request
              </Button>
            </Link>
          </div>
        </PopoverContent>
      </Popover>
    ),
  },
];

const UsersTable = ({ users }: Props) => {
  const [globalFilter, setGlobalSearch] = useState("");
  const [sorting, setSorting] = useState<SortingState>([]);
  const [pagination, setPagination] = useState<PaginationState>({ pageIndex: 0, pageSize: 10 });
  const [rowSelection, setRowSelection] = useState<RowSelectionState>({});

  const table = useReactTable({
    data: users,
    columns,
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    onSortingChange: setSorting,
    onPaginationChange: setPagination,
    onGlobalFilterChange: setGlobalSearch,
    onRowSelectionChange: setRowSelection,
    state: {
      sorting,
      pagination,
      globalFilter,
      rowSelection,
    },
  });

  return (
    <>
      <div className="mb-3 flex items-center justify-between">
        <Input
          placeholder="Search..."
          value={globalFilter}
          onChange={(e) => setGlobalSearch(e.target.value)}
          className="w-80"
        />
      </div>
      <DataTable table={table} />
    </>
  );
};

export default UsersTable;
