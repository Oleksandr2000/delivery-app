"use client";
import { yupResolver } from "@hookform/resolvers/yup";
import { format } from "date-fns";
import { useRouter } from "next/navigation";
import React from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { toast } from "sonner";
import * as yup from "yup";

import { DatePicker } from "@components/DatePicker";
import { Button } from "@design-system/ui/button";
import { Form, FormControl, FormField, FormItem, FormLabel, FormMessage } from "@design-system/ui/form";
import { Input } from "@design-system/ui/input";
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from "@design-system/ui/select";
import { Textarea } from "@design-system/ui/textarea";
import { createOrder, updateOrder } from "@modules/requests/request.actions";
import { ORDER_TYPES, ORDER_TYPES_METADATA, OrderDTO } from "@modules/requests/request.model";

interface Props {
  defaultValues: OrderDTO;
}

const schema = yup.object({
  fromCity: yup.string().required("Enter dispatch city"),
  toCity: yup.string().required("Enter city of arrival"),
  dispatchDate: yup.date().required("Enter dispatch date"),
  type: yup.mixed<ORDER_TYPES>().oneOf(Object.values(ORDER_TYPES)).required("Choose order type"),
  description: yup.string(),
  senderId: yup.number().required(),
});

const UserOrderForm = ({ defaultValues }: Props) => {
  const { push } = useRouter();
  const isEdit = defaultValues?.id;

  const form = useForm<OrderDTO>({
    defaultValues,
    resolver: yupResolver<Omit<OrderDTO, "id">>(schema),
  });

  const {
    handleSubmit,
    control,
    formState: { isDirty },
  } = form;

  const onSubmit: SubmitHandler<OrderDTO> = async (data) => {
    let res;
    if (!isEdit) {
      res = await createOrder(data);
    } else {
      if (!defaultValues.id) {
        return toast("Provide delivery ID");
      }
      res = await updateOrder(data, defaultValues.id);
    }

    if (!res.error) {
      toast(`Order request has been ${isEdit ? "updated" : "created"}.`, {
        description: `Dispatch date - ${format(data.dispatchDate, "dd MMMM yyyy")}`,
        action: {
          label: "Undo",
          onClick: () => {},
        },
      });
      push(`/users/${defaultValues.senderId}/requests`);
    } else {
      toast(res.error.message);
    }
  };

  return (
    <Form {...form}>
      <form className="flex w-96 flex-col gap-6" onSubmit={handleSubmit(onSubmit)}>
        <FormField
          name="fromCity"
          control={control}
          render={({ field }) => (
            <FormItem>
              <FormLabel>From City</FormLabel>
              <FormControl>
                <Input placeholder="From city" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          name="toCity"
          control={control}
          render={({ field }) => (
            <FormItem>
              <FormLabel>To City</FormLabel>
              <FormControl>
                <Input placeholder="To city" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          name="dispatchDate"
          control={control}
          render={({ field }) => (
            <FormItem>
              <FormLabel>Dispatch Date</FormLabel>
              <FormControl>
                <DatePicker value={field.value} onChange={field.onChange} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          name="type"
          control={control}
          render={({ field }) => (
            <FormItem>
              <FormLabel>Order Type</FormLabel>
              <Select value={field.value || undefined} onValueChange={field.onChange}>
                <FormControl>
                  <SelectTrigger value={field.value || undefined} onChange={field.onChange}>
                    <SelectValue placeholder="Choose order type..." />
                  </SelectTrigger>
                </FormControl>
                <SelectContent>
                  {ORDER_TYPES_METADATA.map(({ value, label }) => (
                    <SelectItem key={value} value={value}>
                      {label}
                    </SelectItem>
                  ))}
                </SelectContent>
              </Select>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          name="description"
          control={control}
          render={({ field }) => (
            <FormItem>
              <FormLabel>Description</FormLabel>
              <FormControl>
                <Textarea {...field} placeholder="Enter description.." />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <div className="flex items-center justify-end gap-2">
          <Button type="submit" variant="default" className="px-5" disabled={!isDirty}>
            Save
          </Button>
        </div>
      </form>
    </Form>
  );
};

export default UserOrderForm;
