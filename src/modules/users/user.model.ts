export interface IUser {
  id: number;
  firstname: string;
  surname: string;
  city: string;
  email: string;
  phone: string;
  createdAt: string;
  updatedAt: string;
}
