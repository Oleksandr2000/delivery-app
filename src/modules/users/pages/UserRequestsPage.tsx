"use server";
import { CircleChevronLeft } from "lucide-react";
import Link from "next/link";

import DeliveryTable from "@modules/requests/components/DeliveryTable";
import OrdersTable from "@modules/requests/components/OrdersTable";
import { fetchUserDeliveries, fetchUserOrders } from "@modules/requests/request.actions";

import { fetchCustomerById } from "../user.actions";

const UserRequestsPage = async ({ params: { id } }: { params: Record<string, string> }) => {
  const [{ data: deliveries }, { data: orders }, { data: user }] = await Promise.all([
    fetchUserDeliveries(+id),
    fetchUserOrders(+id),
    fetchCustomerById(+id),
  ]);

  if (!user || !user[0]) {
    return null;
  }

  return (
    <>
      <div className="mb-8 flex items-center gap-2">
        <Link href="/users">
          <CircleChevronLeft className="h-6 w-6" />
        </Link>
        <h1 className="text-2xl font-semibold capitalize">{`${user[0].firstname} ${user[0].surname} - Requests`}</h1>
      </div>
      <h2 className="mb-3 text-xl font-semibold">User Orders</h2>
      <OrdersTable orders={orders || []} addOrderPath={`/users/${id}/create/order`} customerId={+id} />
      <h2 className="mb-3 mt-6 text-xl font-semibold">User Deliveries</h2>
      <DeliveryTable deliveries={deliveries || []} addDeliveryPath={`/users/${id}/create/delivery`} customerId={+id} />
    </>
  );
};

export default UserRequestsPage;
