"use client";
import { CircleChevronLeft } from "lucide-react";
import Link from "next/link";
import { useParams } from "next/navigation";
import React from "react";

import { getDefaultOrderValues } from "@modules/requests/request.model";

import UserOrderForm from "../components/UserOrderForm";

const CreateUserOrderPage = () => {
  const { id } = useParams();

  return (
    <>
      <div className="mb-8  flex items-center gap-2">
        <Link href={`/users/${id}/requests`}>
          <CircleChevronLeft className="h-6 w-6" />
        </Link>
        <h1 className="text-2xl font-semibold">Add Order</h1>
      </div>
      <UserOrderForm defaultValues={getDefaultOrderValues(+id)} />
    </>
  );
};

export default CreateUserOrderPage;
