"use server";
import supabase from "@services/supabase.service";

import UsersTable from "../components/UsersTable";

export const UsersListPage = async () => {
  const { data } = await supabase.from("customers").select("*");

  return (
    <>
      <h1 className="text-2xl font-semibold">Customers</h1>
      <div className="mt-6">
        <UsersTable users={data || []} />
      </div>
    </>
  );
};

export default UsersListPage;
