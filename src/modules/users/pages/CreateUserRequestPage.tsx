"use client";
import { PackageCheck, Truck } from "lucide-react";
import Link from "next/link";
import { useParams } from "next/navigation";
import React from "react";

import { Button } from "@design-system/ui/button";
import { Card, CardContent, CardFooter, CardHeader, CardTitle } from "@design-system/ui/card";

const CreateUserRequestPage = () => {
  const { id } = useParams();

  return (
    <div className="flex min-h-screen items-center justify-center gap-8">
      <Card>
        <CardHeader>
          <div className="flex items-center gap-2">
            <Truck className="h-6 w-6" />
            <CardTitle>Delivery</CardTitle>
          </div>
        </CardHeader>
        <CardContent>
          <p className="text-sm font-normal text-muted-foreground opacity-60">Create delivery request for a user.</p>
        </CardContent>
        <CardFooter>
          <Link href={`/users/${id}/create/delivery`}>
            <Button variant="outline">Create delivery</Button>
          </Link>
        </CardFooter>
      </Card>
      <Card>
        <CardHeader>
          <div className="flex items-center gap-2">
            <PackageCheck className="h-6 w-6" />
            <CardTitle>Order</CardTitle>
          </div>
        </CardHeader>
        <CardContent>
          <p className="text-sm font-normal text-muted-foreground opacity-60">Create order request for a user.</p>
        </CardContent>
        <CardFooter>
          <Link href={`/users/${id}/create/order`}>
            <Button variant="outline">Create order</Button>
          </Link>
        </CardFooter>
      </Card>
    </div>
  );
};

export default CreateUserRequestPage;
