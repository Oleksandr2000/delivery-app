"use server";
import { CircleChevronLeft } from "lucide-react";
import Link from "next/link";
import React from "react";

import { fetchDeliveryById } from "@modules/requests/request.actions";
import { normalizeDelivery } from "@modules/requests/request.utils";

import UserDeliveryForm from "../components/UserDeliveryForm";

const EditUserDeliveryPage = async ({ params: { rid, id } }: { params: Record<string, string> }) => {
  const { data: delivery } = await fetchDeliveryById(+rid);

  if (!delivery || !delivery[0]) {
    return null;
  }

  return (
    <>
      <div className="mb-8 flex items-center gap-2">
        <Link href={`/users/${id}/requests`}>
          <CircleChevronLeft className="h-6 w-6" />
        </Link>
        <h1 className="text-2xl font-semibold">Edit Delivery</h1>
      </div>
      <UserDeliveryForm defaultValues={normalizeDelivery(delivery[0])} />
    </>
  );
};

export default EditUserDeliveryPage;
