"use server";
import { CircleChevronLeft } from "lucide-react";
import Link from "next/link";
import React from "react";

import { fetchOrderById } from "@modules/requests/request.actions";
import { normalizeOrder } from "@modules/requests/request.utils";

import UserOrderForm from "../components/UserOrderForm";

const EditUserOrderPage = async ({ params: { rid, id } }: { params: Record<string, string> }) => {
  const { data: order } = await fetchOrderById(+rid);

  if (!order || !order[0]) {
    return null;
  }

  return (
    <>
      <div className="mb-8 flex items-center gap-2">
        <Link href={`/users/${id}/requests`}>
          <CircleChevronLeft className="h-6 w-6" />
        </Link>
        <h1 className="text-2xl font-semibold">Edit Order</h1>
      </div>
      <UserOrderForm defaultValues={normalizeOrder(order[0])} />
    </>
  );
};

export default EditUserOrderPage;
