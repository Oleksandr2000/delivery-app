"use client";
import { flexRender, Table as ReactTable } from "@tanstack/react-table";

import { Button } from "@design-system/ui/button";
import { Table, TableBody, TableCell, TableHead, TableHeader, TableRow } from "@design-system/ui/table";

function DataTable<TData>({ table }: { table: ReactTable<TData> }) {
  const selectedCount = table.getSelectedRowModel().rows.length;

  return (
    <div className="rounded-md border shadow-md">
      <Table>
        <TableHeader>
          {table.getHeaderGroups().map((headerGroup, headerGroupIndex) => (
            <TableRow key={headerGroup.id + headerGroupIndex} className="bg-secondary hover:bg-secondary">
              {headerGroup.headers.map((header) => {
                return (
                  <TableHead key={header.id + header.index} className="first:rounded-tl-md last:rounded-tr-md">
                    {header.isPlaceholder ? null : flexRender(header.column.columnDef.header, header.getContext())}
                  </TableHead>
                );
              })}
            </TableRow>
          ))}
        </TableHeader>
        <TableBody>
          {table.getRowModel().rows?.length ? (
            table.getRowModel().rows.map((row, rowIndex) => (
              <TableRow key={row.id + rowIndex} data-state={row.getIsSelected() && "selected"}>
                {row.getVisibleCells().map((cell, cellIndex) => (
                  <TableCell key={cell.id + row.id + cellIndex}>
                    {flexRender(cell.column.columnDef.cell, cell.getContext())}
                  </TableCell>
                ))}
              </TableRow>
            ))
          ) : (
            <TableRow>
              <TableCell colSpan={table.getAllColumns().length} className="h-24 text-center">
                No results.
              </TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
      <div className="flex items-center justify-between p-3">
        <p className="text-sm text-muted-foreground opacity-50">
          {selectedCount ? `${selectedCount} ${selectedCount > 1 ? "rows" : "row"} selected` : "No selected rows"}
        </p>

        <div className="flex items-center justify-end gap-2">
          <Button
            variant="outline"
            size="sm"
            onClick={() => table.previousPage()}
            disabled={!table.getCanPreviousPage()}
          >
            Previous
          </Button>
          <Button variant="outline" size="sm" onClick={() => table.nextPage()} disabled={!table.getCanNextPage()}>
            Next
          </Button>
        </div>
      </div>
    </div>
  );
}

export default DataTable;
